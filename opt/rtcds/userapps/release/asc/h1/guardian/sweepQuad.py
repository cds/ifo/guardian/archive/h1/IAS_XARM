# sweepQuad.py
#
# $Id: sweepQuad.py 8898 2014-10-15 21:55:45Z kiwamu.izumi@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/sweepQuad.py $

#============================================
# this is a function to search for max power

from numpy import argmax
from time import sleep


# Function to search for max power
def sweepQuad(optic, PdChannel, angle):
    # optic example: 'ITMX'
	# PdChannel example: 'AOS-ETMX_BAFFLEPD_1_POWER'
	# angle in set in urad

	# Initial angles
	initAngleP = ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OFFSET']
	initAngleY = ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OFFSET']

	# Set ramp times
	ezca['SUS-' + optic + '_M0_OPTICALIGN_P_TRAMP'] = 15
	ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_TRAMP'] = 15

	# Get calibration
	calP = ezca['SUS-' + optic + '_M0_OPTICALIGN_P_GAIN']
	calY = ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_GAIN']

	# Search YAW
	# Go left
	ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OFFSET'] = initAngleY - angle

	# Get data
	dataAngle = []
	dataPD    = []
	while ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OUTPUT'] > (initAngleY - angle) * calY + 1e-2:
		dataAngle.append(ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OUTPUT'] / calY)
		dataPD.append(ezca[PdChannel])

	# Go right
	ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OFFSET'] = initAngleY + angle

	# Get data
	while ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OUTPUT'] < (initAngleY + angle) * calY - 1e-2:
                dataAngle.append(ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OUTPUT'] / calY)
                dataPD.append(ezca[PdChannel])

	# Find max power
	peakY = argmax(dataPD)
	
	# Set yaw position
	ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_TRAMP'] = 8
	ezca['SUS-' + optic + '_M0_OPTICALIGN_Y_OFFSET'] = dataAngle[peakY]
	sleep(10)

	# Search PITCH
        # Go left
        ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OFFSET'] = initAngleP - angle

        # Get data
        dataAngle = []
        dataPD    = []
        while ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OUTPUT'] > (initAngleP - angle) * calP + 1e-2:
                dataAngle.append(ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OUTPUT'] / calP)
                dataPD.append(ezca[PdChannel])

        # Go right
        ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OFFSET'] = initAngleP + angle

        # Get data
        while ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OUTPUT'] < (initAngleP + angle) * calP - 1e-2:
                dataAngle.append(ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OUTPUT'] / calP)
                dataPD.append(ezca[PdChannel])

        # Find max power
        peakP = argmax(dataPD)

        # Set pitch position
	ezca['SUS-' + optic + '_M0_OPTICALIGN_P_TRAMP'] = 8
        ezca['SUS-' + optic + '_M0_OPTICALIGN_P_OFFSET'] = dataAngle[peakP]
	sleep(10)

        return dataPD[peakP]
