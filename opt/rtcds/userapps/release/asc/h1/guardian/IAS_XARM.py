#   IAS_XARM.py
#
# $Id: IAS_XARM.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IAS_XARM.py $
#

from guardian import GuardState
from math import sqrt
import IASstates
import ias_quad_sus, gen_IAS_STATES
import time

# initial request on initialization
request = 'IDLE'

global ITMX_BPD1_P
global ITMX_BPD1_Y


# IDLE
class IDLE(GuardState):
    request = True
    goto = True
    def main(self):
        global ITMX_BPD1_P
        global ITMX_BPD1_Y
        global ITMY_BPD1_P
        global ITMY_BPD1_Y

        # initialize the global values
        ITMX_BPD1_P = 100 
        ITMX_BPD1_Y = 100
        ITMY_BPD1_P = 100
        ITMY_BPD1_Y = 100
        return


# align TMSX
ALIGN_TMSX = gen_IAS_STATES.gen_ALIGN_TMSXY(arm='X',
			camera_name = 'VID-CAM22',
            itmCameraX = 'VID-CAM22_X',
            itmCameraY = 'VID-CAM22_Y',
            xPosTarget = 391.0,
            yPosTarget = 267.0,
            xCal = -3.25,
            yCal = -2.5,
            mu = 0.1,
            GoodError = 0.5, # in pixels
            exp_time = 100000)


## TMSX aligned
class TMSX_ALIGNED(GuardState):
    request = True
    def main(self):
        log('TMS aligned')
        pass
    

# make an instance
POINT_ITMX_TO_BPD1 = gen_IAS_STATES.gen_POINT_ITM_TO_BPD(arm = 'X',
                    sus='ITMX',
                    PDnum=1,
                    itmOffsetPitch = -372.7,
                    itmOffsetYaw = -857.7,
                    SusMove = 3)



# find max power on BPD1
FIND_MAX_BPD1POWER = gen_IAS_STATES.gen_FIND_MAX_BPDPOWER(arm ='X',
                    PDnum = 1,
                    GoodPower = 0.3)



# POINT ITMX TO BPD4
POINT_ITMX_TO_BPD4 = gen_IAS_STATES.gen_POINT_ITM_TO_BPD(arm = 'X',
                    sus='ITMX',
                    PDnum=4,
                    itmOffsetPitch = 372.7,
                    itmOffsetYaw = 857.7,
                    SusMove = 6)

# check the power on the second baffle PD
CHECK_SECOND_BPDX = gen_IAS_STATES.gen_CHECK_SECOND_BPD(arm = 'X',
                    PDnum = 4,
                    GoodPower = 0.1)

# ITMX aligned
class ITMX_ALIGNED(GuardState):
    request = True
    def main (self):
        pass

#ALIGN_ETMX
ALIGN_ETMX = gen_IAS_STATES.gen_ALIGN_ETM(arm='X',
            itmCameraX = 'VID-CAM22_X',
            itmCameraY = 'VID-CAM22_Y',
            xPosTarget = 391.0,
            yPosTarget = 267.0,
            xCal = -3.25,
            yCal = -2.5,
            mu = 0.1,
            GoodError = 0.5, # in pixels
            exp_time = 100000)


#ALIGN_ETMX
ALIGN_ETMX_GREEN_LOCKED = gen_IAS_STATES.gen_ALIGN_ETM_GREEN_LOCKED(arm='X',
            itmCameraX = 'VID-CAM22_X',
            itmCameraY = 'VID-CAM22_Y',
            xPosTarget = 391.0,
            yPosTarget = 267.0,
            xCal = -3.25,
            yCal = -2.5,
            mu = 0.1,
            GoodError = 0.5) # in pixels

# MAXIMIZE TR
MAXIMIZE_TRX = gen_IAS_STATES.gen_MAXIMIZE_TR(arm='X')


# ETMX_ALIGNED
class ETMX_ALIGNED(GuardState):
    request = True
    def main(self):
        pass


#[FIXME] make these classes generators
# let's align the differential angle
class ALIGN_DIFF_ANGLES (GuardState):
    request = True
    def main(self):
        # make an instance of x arm mirrors
        xarm = ias_quad_sus.QUADsuspension('ITMX','ETMX')

        # do the scan in yaw
        best_angles_yaw = xarm.dual_sweep(sweep_range = 1.0,
                        PDchan =  'ALS-X_TR_A_LF_OUTPUT',
                        PorY = 'Y',
                        B_gain = -1.0)  

        # set the angles for yaw
        xarm.set_angle('ITMX', 'Y', best_angles_yaw[0])
        xarm.set_angle('ETMX', 'Y', best_angles_yaw[1])

        # wait 
        while ezca.get_LIGOFilter('SUS-ITMX_M0_OPTICALIGN_Y').is_offset_ramping():
            log('waiting for suspensions to settle down ...')
            time.sleep(0.5)
        
        # do the scan in pitch
        best_angles_pit = xarm.dual_sweep(sweep_range = 1.0,
                        PDchan =  'ALS-X_TR_A_LF_OUTPUT',
                        PorY = 'P',
                        B_gain = -1.0)  

        # set the angles for pit
        xarm.set_angle('ITMX', PorY='P', value=best_angles_pit[0])
        xarm.set_angle('ETMX', PorY='P', value=best_angles_pit[1])

    def run(self):
        if ezca.get_LIGOFilter('SUS-ITMX_M0_OPTICALIGN_P').is_offset_ramping():
            return True

# align the common angle
class ALIGN_COMM_ANGLES(GuardState):
    request = True
    def main(self):
        # make an instance of x arm mirrors
        xarm = ias_quad_sus.QUADsuspension('ITMX','ETMX')

        # do the scan in yaw
        best_angles_yaw = xarm.dual_sweep(sweep_range = 1.0,
                        PDchan =  'ALS-X_TR_A_LF_OUTPUT',
                        PorY = 'Y',
                        B_gain = 1.0)   

        # set the angles for yaw
        xarm.set_angle('ITMX', 'Y', best_angles_yaw[0])
        xarm.set_angle('ETMX', 'Y', best_angles_yaw[1])

        # wait 
        while ezca.get_LIGOFilter('SUS-ITMX_M0_OPTICALIGN_Y').is_offset_ramping():
            log('waiting for suspensions to settle down ...')
            time.sleep(0.5)
        
        # do the scan in pitch
        best_angles_pit = xarm.dual_sweep(sweep_range = 1.0,
                        PDchan =  'ALS-X_TR_A_LF_OUTPUT',
                        PorY = 'P',
                        B_gain = 1.0)   

        # set the angles for pit
        xarm.set_angle('ITMX', PorY='P', value=best_angles_pit[0])
        xarm.set_angle('ETMX', PorY='P', value=best_angles_pit[1])

    def run(self):
        if ezca.get_LIGOFilter('SUS-ITMX_M0_OPTICALIGN_P').is_offset_ramping():
            return True


#
class ETMX_ITMX_ALIGNED(GuardState):
    request = True
    def main(self):
        log('done aligning ITMX and ETMX')
        pass

##################################################
edges = [
#ETMX
    ('IDLE', 'ALIGN_ETMX'),
    ('ALIGN_ETMX', 'ALIGN_ETMX_GREEN_LOCKED'),
    ('ALIGN_ETMX_GREEN_LOCKED', 'MAXIMIZE_TRX'),
    ('MAXIMIZE_TRX', 'ETMX_ALIGNED'),
#ITMX
    ('IDLE', 'POINT_ITMX_TO_BPD1'),
    ('POINT_ITMX_TO_BPD1', 'FIND_MAX_BPD1POWER'),
    ('FIND_MAX_BPD1POWER', 'POINT_ITMX_TO_BPD4'),
    ('POINT_ITMX_TO_BPD4', 'CHECK_SECOND_BPDX'),
    ('CHECK_SECOND_BPDX', 'ITMX_ALIGNED'),
#TMS
    ('IDLE', 'ALIGN_TMSX'),
    ('ALIGN_TMSX', 'TMSX_ALIGNED'),
# diff/comm techniques
    ('IDLE', 'ALIGN_DIFF_ANGLES'),
    ('ALIGN_DIFF_ANGLES', 'ALIGN_COMM_ANGLES'),
    ('ALIGN_COMM_ANGLES', 'ETMX_ITMX_ALIGNED'),
    
]

