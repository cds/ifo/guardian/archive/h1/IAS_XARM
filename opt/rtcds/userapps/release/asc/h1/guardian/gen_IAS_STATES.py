# Initial Alignment
#
# $Id: IASstates.py 8898 2014-10-15 21:55:45Z kiwamu.izumi@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IASstates.py $
#

from guardian import GuardState, GuardStateDecorator
from math import sqrt, pow
from time import sleep
from numpy import argmax
import cdsutils

# load the sweepQuad function which resides in /sys/l1/guardian
from sweepQuad import sweepQuad


########################################################
# the global ITM alignment values
# ITM needs this kind of global values because the alignment
# process is multiple-steps rather than a single step.
def set_globals(arm, pit, yaw):
    if arm == 'X':
        global ITMX_BPD1_P
        global ITMX_BPD1_Y
        ITMX_BPD1_P = pit
        ITMX_BPD1_Y = yaw
    elif arm == 'Y':
        global ITMY_BPD1_P
        global ITMY_BPD1_Y
        ITMY_BPD1_P = pit
        ITMY_BPD1_Y = yaw


# sleep function, combined with a log message
def mysleep(tsleep):
    log('######## sleep for %d sec ...' % (tsleep))
    sleep(tsleep)


#################################################
# centering function using GigE camera
#################################################
def GigE_centering(actuation_name, sensor_name, target_value, rate_value): 
    # move actuation point
    ezca[actuation_name] += rate_value * (target_value - ezca[sensor_name])
    # return the distance to the target
    return ezca[sensor_name] - target_value


##################################################
# TMSX(Y) alignment using ITMX(Y) Camera
##################################################
# user should pre-define the following variables when making an instance
# yPosTarget
# xPosTarget
# arm
# xCal
# yCal
# mu
# GoodError
def gen_ALIGN_TMSXY(arm, camera_name, itmCameraX, itmCameraY, xPosTarget, yPosTarget, xCal, yCal,mu, GoodError, exp_time):
    class ALIGN_TMSXY(GuardState):
        request = False

        def main(self):
            # set variables
            self.arm = arm
            self.itmCameraX = itmCameraX
            self.itmCameraY = itmCameraY
            self.xCal = xCal
            self.yCal = yCal
            self.xPosTarget = xPosTarget
            self.yPosTarget = yPosTarget
            self.mu = mu
            self.GoodError = GoodError

            # define some channels names and etc
            self.tmsTrampPit = 'SUS-TMS%s_M1_OPTICALIGN_P_TRAMP' %(self.arm)
            self.tmsTrampYaw = 'SUS-TMS%s_M1_OPTICALIGN_Y_TRAMP' %(self.arm)
            self.tmsBiasPit = 'SUS-TMS%s_M1_OPTICALIGN_P_OFFSET' %(self.arm)
            self.tmsBiasYaw = 'SUS-TMS%s_M1_OPTICALIGN_Y_OFFSET' %(self.arm)

            # Set camera exposure
            ezca[camera_name + '_EXP'] = exp_time

            # Set up ramping time to 1 sec before updating the offsets
            ezca[self.tmsTrampPit] = 1
            ezca[self.tmsTrampYaw] = 1

            # Initialize position error, pix, timer
            self.error = 10
            self.timer['TMSmove'] = 0
            self.NoTRIALs = 0

        def run(self):
            if self.timer['TMSmove']:  # Allow time for TMS to move ( 1sec default)
                # move the spot on ITM 
                self.errorx = GigE_centering(self.tmsBiasYaw,       # actuation point
                                             self.itmCameraX,       # error signal
                                             self.xPosTarget,       # target value
                                             (self.mu)/(self.xCal)  # calibration and adoption rate
                                              )
                self.errory = GigE_centering(self.tmsBiasPit,       # actuation point
                                             self.itmCameraY,       # error signal
                                             self.yPosTarget,       # target value
                                             (self.mu)/(self.yCal)  # calibration and adoption rate
                                              )

                # Compute distance to the target
                self.error = sqrt( pow(self.errorx, 2) + pow(self.errory, 2))

                # Allow time for TMSX to move
                self.timer['TMSmove'] = 1
                log('######### Current distance to the target is %.5f pixels' %
                    (self.error))
                self.NoTRIALs += 1

            # exit this state if error is smaller than 1 pixel
            if self.error <= self.GoodError:
                return True
            # exit when the pixel does not converge
            if self.NoTRIALs > 200:
                notify('ERROR: the number of iterations exceeded 200.')
                return 'FAULT'

    return ALIGN_TMSXY

#################################################################
# pinting ITM to Baffle PD
#################################################################
# do not forget to set self.arm, self.PDnum and self.SUSmove when making an instance
def gen_POINT_ITM_TO_BPD(arm, sus, PDnum, itmOffsetPitch, itmOffsetYaw, SusMove):
    class POINT_ITM_TO_BPD(GuardState):
        request = False

        def main(self):
            # initi variables
            self.arm = arm
            self.sus = sus
            self.PDnum = PDnum
            self.itmOffsetPitch = itmOffsetPitch
            self.itmOffsetYaw = itmOffsetYaw
            self.SusMove = SusMove
            
            # set channel names. Note that self.arm must be speficied when instanciated
            self.itm = 'ITM%s'%(self.arm)
            self.itmM0 = 'SUS-ITM%s_M0_'%(self.arm)
            self.itmBiasPit = 'SUS-ITM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.itmBiasYaw = 'SUS-ITM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.itmTestBiasPit = 'SUS-ITM%s_M0_TEST_P_OFFSET'%(self.arm)
            self.itmTestBiasYaw = 'SUS-ITM%s_M0_TEST_Y_OFFSET'%(self.arm)
            self.BPD1power = 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum)
            
            log('########  setting up %s'%(self.sus))
            # Set offset ramp times
            ezca[self.itmM0 + 'TEST_P_TRAMP'] = 5
            ezca[self.itmM0 + 'TEST_Y_TRAMP'] = 5
            ezca[self.itmM0 + 'OPTICALIGN_P_TRAMP'] = 1
            ezca[self.itmM0 + 'OPTICALIGN_Y_TRAMP'] = 1

            # baffle PD
            log('######## Pointing %s to BPD%d'%(self.sus, self.PDnum))
            ezca[self.itmTestBiasPit] = self.itmOffsetPitch
            ezca[self.itmTestBiasYaw] = self.itmOffsetYaw
            ezca.switch(self.itmM0 + 'TEST_P', 'OFFSET', 'OUTPUT', 'ON')
            ezca.switch(self.itmM0 + 'TEST_Y', 'OFFSET', 'OUTPUT', 'ON')

            # waite for SUS to move. SusMove must be specified when instanciated
            self.timer['sus to move'] = self.SusMove

        def run (self):
            if self.timer['sus to move']:
                return True

    return POINT_ITM_TO_BPD

#################################################################
# finding max power in a baffle pd
#################################################################
def gen_FIND_MAX_BPDPOWER(arm, PDnum, GoodPower):
    class FIND_MAX_BPDPOWER(GuardState):
        request = False

        def main(self):
            # set variables
            self.arm = arm
            self.PDnum = PDnum
            self.GoodPower = GooPower

            # define some names which are used frequently
            self.sus = 'ITM%s'%(self.arm)
            self.susBiasPit = 'SUS-ITM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ITM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.BPDchannel = 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum)
            
            log('######## Finding max point on BPD ... ')
            self.powerBPD = sweepQuad(self.sus, self.BPDchannel, 3)
            log('########  PD power is at %f mW'%(self.powerBPD))
    
            # Read current TMSX offsets
            if self.arm == 'X':
                global ITMX_BPD1_P
                global ITMX_BPD1_Y
                ITMX_BPD1_P = ezca[self.susBiasPit]
                ITMX_BPD1_Y = ezca[self.susBiasYaw]
            elif self.arm == 'Y':
                global ITMY_BPD1_P
                global ITMY_BPD1_Y
                ITMY_BPD1_P = ezca[self.susBiasPit]
                ITMY_BPD1_Y = ezca[self.susBiasYaw]
                log('### BPD1 maximized when ITMY pit is at %f'%(ITMY_BPD1_P))
                log('### BPD1 maximized when ITMY yaw is at %f'%(ITMY_BPD1_Y))

            if self.powerBPD < self.GoodPower:
                notify('BAFFLEPD%d is too low! Align %s manually'%(self.PDnum, self.sus))
                return 'MANUAL_ITM%s'%(self.arm)
            return True
    return FIND_MAX_BPDPOWER
                
#################################################################
# check the power on a second baffle PD
#################################################################
def gen_CHECK_SECOND_BPD(arm, PDnum, GoodPower):
    class CHECK_SECOND_BPD(GuardState):
        request = False

        def main(self):
            # set variables
            self.arm = arm
            self.PDnum = PDnum
            self.GoodPower = GoodPower


            # define some names which are used frequently
            #[FIXME] can we avoid repeating calling these ?
            self.sus = 'ITM%s'%(self.arm)
            self.susM0 = 'SUS-ITM%s_M0_'%(self.arm)
            self.susBiasPit = 'SUS-ITM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ITM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.BPDchannel = 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum)
            
            log('######## Align %s on '%(self.sus))

            # Read current TMS offsets
            self.BPD2Pitch = ezca[self.susBiasPit]
            self.BPD2Yaw = ezca[self.susBiasYaw]

            if self.arm == 'X':
                global ITMX_BPD1_P
                global ITMX_BPD1_Y
                self.BPD1Pitch = ITMX_BPD1_P
                self.BPD1Yaw = ITMX_BPD1_Y
            elif self.arm == 'Y':
                global ITMY_BPD1_P
                global ITMY_BPD1_Y
                self.BPD1Pitch = ITMY_BPD1_P
                self.BPD1Yaw = ITMY_BPD1_Y
                log('BPD1_P = %f'%(ITMY_BPD1_P))
                log('BPD1_P = %f'%(ITMY_BPD1_Y))
         
            # check the BPD power
            self.BPDpower = cdsutils.avg(1, 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum))
            log('BPD power is at %f'%(self.BPDpower))

            # move ITM to a good position
            ezca[self.susBiasPit] = self.BPD1Pitch
            ezca[self.susBiasYaw] = self.BPD1Yaw

            # check the power on the PD 
            if self.BPDpower > self.GoodPower: 
                # Centering on the optic
                log('BPD power is good at %f'%(self.BPDpower))
            else:
                notify('PD power is too low !')
                log('PD power is loo low. It is now %f'%(self.BPDpower))

            # Turn off test offsets
            ezca.switch(self.susM0 + 'TEST_P', 'OFFSET', 'OFF')
            ezca.switch(self.susM0 + 'TEST_Y', 'OFFSET', 'OFF')
            return True
    return CHECK_SECOND_BPD


#################################################################
# Manual Alignment of ITMX
#################################################################
def generate_MANUAL_ITM():
    class MANUAL_ITM_GEN(GuardState):
        request = False

        def run(self):
            self.BPDpower = cdsutils.avg(1, 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum))
            if self.BPDpower >= self.GoodPower:
                return 'POINT_ITM%s_TO_BPD1'%(self.arm)

    return MANUAL_ITM_GEN


#################################################################
# ETM alignment using ITMX Camera without green locked
#################################################################
def gen_ALIGN_ETM(arm, itmCameraX, itmCameraY, xPosTarget, yPosTarget, xCal, yCal, mu, GoodError, exp_time ):
    class ALIGN_ETM(GuardState):
        request = False

        def main(self):
            # set variables
            self.arm = arm
            self.itmGreenX = xPosTraget
            self.itmGreenY = yPosTarget
            self.error = 10       # initial error
            self.GoodError = GoodError
            self.mu = mu
            self.xCal = xCal
            self.yCal = yCal
            self.itmCameraX = itmCameraX
            self.itmCameraY = itmCameraY


            # define names
            self.sus = 'ETM%s'%(self.arm)
            self.susM0 = 'SUS-ETM%s_M0_'%(self.arm)
            self.susBiasPit = 'SUS-ETM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ETM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.itmCameraX = itmCameraX
            self.itmCameraY = itmCameraY

            # Set camera exposure to 30000
            ezca['CAM-ITMX_EXP'] = exp_time

            # Set up ramping time to 1 sec before updating the offsets
            ezca[self.susM0 + 'OPTICALIGN_Y_TRAMP'] = 1
            ezca[self.susM0 + 'OPTICALIGN_P_TRAMP'] = 1

            log('######## Turn off greed PDH servo')
            ezca['ALS-%s_REFL_SERVO_IN1EN'%(self.arm)] = 0

            # Initialize position error, pix, # of trials, coarseness
            self.timer['ETM to move'] = 0
            self.NoTRIALs = 0
            self.coarseness = 'COARSE'

        def run(self):
            if self.timer['ETM to move']:
                # Allow time for ETMX to move
                self.timer['ETM to move'] = 1

                # Update YAW
                self.errorx = GigE_centering(self.susBiasYaw,       # actuation point
                                             self.itmCameraX,       # error signal
                                             self.itmGreenX,       # target value
                                             (self.mu)/(self.xCal)  # calibration and adoption rate
                                              )
                # Update PIT
                self.errory = GigE_centering(self.susBiasPit,       # actuation point
                                             self.itmCameraY,       # error signal
                                             self.itmGreenY,       # target value
                                             (self.mu)/(self.yCal)  # calibration and adoption rate
                                              )

                # Compute distance to the target
                self.error = sqrt(pow(self.errorx, 2) + pow(self.errory, 2))
                log('Current distance to the target is %.5f pixels' % (self.error))
                self.NoTRIALs += 1

                # switch the coarseness if error is smaller than GoodError
            if self.error <= self.GoodError:
                return True
 
            if self.NoTRIALs > 200:
                notify('Number of trials exceeded 200 times.')
                return 'FAULT'
    return ALIGN_ETM



#################################################################
# ETM alignemnt with green locked
#################################################################
def gen_ALIGN_ETM_GREEN_LOCKED(arm, itmCameraX, itmCameraY, xPosTarget, yPosTarget, xCal, yCal, mu, GoodError):
    class ALIGN_ETM_GREEN_LOCKED(GuardState):
        request = False
        def main(self):
            # set variables
            self.arm = arm
            self.itmGreenX = xPosTarget
            self.itmGreenY = yPosTarget
            self.error = 5     # initial error
            self.GoodError = GoodError
            self.mu = mu
            self.xCal = xCal
            self.yCal = yCal


            # define names
            self.sus = 'ETM%s'%(self.arm)
            self.susM0 = 'SUS-ETM%s_M0_'%(self.arm)
            self.susBiasPit = 'SUS-ETM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ETM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.itmCameraX ='CAM-ITM%s_X'%(self.arm)
            self.itmCameraY ='CAM-ITM%s_Y'%(self.arm)

            log('######## Turn on greed PDH servo')
            ezca['ALS-%s_REFL_SERVO_IN1EN'%(self.arm)] = 1

            # Reduce adaptation rate
            self.timer['ETMmove'] = 5
            self.NoTRIALs = 0

        def run(self):
            if self.timer['ETMmove']:

                # Update YAW
                self.errorx = GigE_centering(self.susBiasYaw,       # actuation point
                                             self.itmCameraX,       # error signal
                                             self.itmGreenX,       # target value
                                             (self.mu)/(self.xCal)  # calibration and adoption rate
                                              )
                # Update PIT
                self.errory = GigE_centering(self.susBiasPit,       # actuation point
                                             self.itmCameraY,       # error signal
                                             self.itmGreenY,       # target value
                                             (self.mu)/(self.yCal)  # calibration and adoption rate
                                              )

                # Compute distance to the target
                self.error = sqrt(pow(self.errorx, 2) + pow(self.errory, 2))
                log('Current distance to the target is %.5f pixels' % (self.error))

                # Allow time for ETMX to move
                self.timer['ETMmove'] = 1
                self.NoTRIALs += 1

            # finsh the alignment when error is less than GoodError
            if self.error <= self.GoodError:
                return True

            # go to FAULT
            if self.NoTRIALs > 200:
                notify('Number of trials exceeded 200 times.')
                return 'FAULT'
    return ALIGN_ETM_GREEN_LOCKED

#################################################################
# maximize the green TR
#################################################################
def gen_MAXIMIZE_TR(arm):
    class MAXIMIZE_TR(GuardState):
            request = False
            def main(self):
                self.sus = 'ETM%s'%(self.arm)
                log('maximizing power . . . ')
                # power optimization
                sweepQuad(self.sus, 'ALS-C_TR%s_A_LF_OUTPUT'%(self.arm), 0.5)
                return True
    return MAXIMIZE_TR


