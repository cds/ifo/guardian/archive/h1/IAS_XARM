# Initial Alignment
#
# $Id: IASstates.py 9008 2014-10-30 18:17:06Z jameson.rollins@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IASstates.py $
#

from guardian import GuardState, GuardStateDecorator
from math import sqrt, pow
from time import sleep
import subprocess
from numpy import argmax
import cdsutils

# load the sweepQuad function which resides in /sys/l1/guardian
from sweepQuad import sweepQuad

# import the IMC locking checker
#from IFO_LOCK import MC_is_locked

# existing handy scripts
pr2im4_alignment = '/opt/rtcds/userapps/trunk/asc/l1/scripts/initial_alignment/xarm_alignment.py'
prx_alignment = '/opt/rtcds/userapps/trunk/asc/l1/scripts/initial_alignment/prx_alignment.py'
bs_alignment = '/opt/rtcds/userapps/trunk/asc/l1/scripts/initial_alignment/bs_alignment.py'
sry_alignment = '/opt/rtcds/userapps/trunk/asc/l1/scripts/initial_alignment/sry_alignment.py'
restoreSUS = "/opt/rtcds/userapps/trunk/sus/common/scripts/align_restore"
align_save = '/opt/rtcds/userapps/trunk/sus/common/scripts/align_save'

# sus burt files
susburt = '/opt/rtcds/userapps/release/sus/l1/burtfiles/'

def MC_is_locked():
    return ezca.read('GRD-IMC_LOCK_STATE', as_string=True) == 'LOCKED'

########################################################
# the global ITM alignment values
# ITM needs this kind of global values because the alignment
# process is multiple-steps rather than a single step.
def set_globals(arm, pit, yaw):
    if arm == 'X':
        global ITMX_BPD1_P
        global ITMX_BPD1_Y
        ITMX_BPD1_P = pit
        ITMX_BPD1_Y = yaw
    elif arm == 'Y':
        global ITMY_BPD1_P
        global ITMY_BPD1_Y
        ITMY_BPD1_P = pit
        ITMY_BPD1_Y = yaw

# set BS oplev threshold values
def set_bs_oplev_limit(pit, yaw):
    global BS_OPLEV_PIT_HIGH
    global BS_OPLEV_YAW_HIGH
    BS_OPLEV_PIT_HIGH = pit
    BS_OPLEV_YAW_HIGH = pit

# Determine if XARM is locked
def arm_locked(which, trig):
    return cdsutils.avg(3, 'LSC-%s_TR_A_LF_OUTPUT'%(which)) > trig

# sleep function, combined with a log message
def mysleep(tsleep):
    log('######## sleep for %d sec ...' % (tsleep))
    sleep(tsleep)

# misalign optic (This is used by IFO_ALIGN guardian.)
# concept is that each small guardian does not align or misalign optics but does
# run some initial alignment processes
def misalign_optic(sus, angle=100):
    log('misaligning %s ...'%(sus))
    # for quad suspensions
    if len(sus) == 4:
        ezca['SUS-' + sus + '_M0_OPTICALIGN_P_TRAMP'] = 8
        ezca['SUS-' + sus + '_M0_OPTICALIGN_Y_TRAMP'] = 8
        ezca['SUS-' + sus + '_M0_OPTICALIGN_P_OFFSET'] += angle
        ezca['SUS-' + sus + '_M0_OPTICALIGN_Y_OFFSET'] += angle
    # for everyone else
    else:
        ezca['SUS-' + sus + '_M1_OPTICALIGN_P_TRAMP'] = 8
        ezca['SUS-' + sus + '_M1_OPTICALIGN_Y_TRAMP'] = 8
        ezca['SUS-' + sus + '_M1_OPTICALIGN_P_OFFSET'] += angle
        ezca['SUS-' + sus + '_M1_OPTICALIGN_Y_OFFSET'] += angle

#################################################
# a stupid way of getting the suspension aligned values
#################################################
def read_sus_aligned_values(SUS):
    burtfile = susburt + SUS.lower() + '/' + SUS +'_alignment_values.txt'
    f = open(burtfile, 'r')
    for line in f:
        sentence = line
    pit = float(sentence.split()[5])
    yaw = float(sentence.split()[6])
    return [pit, yaw]

#################################################
# a stupid way of checking the suspension state
#################################################
def is_aligned (SUS):
    [pit_good, yaw_good] = read_sus_aligned_values(SUS)
    
    # for quad SUSpensions
    if len(SUS) == 4: 
        currentP = ezca['SUS-' + SUS + '_M0_OPTICALIGN_P_OFFSET']
        currentY = ezca['SUS-' + SUS + '_M0_OPTICALIGN_Y_OFFSET']
    # for everyone else
    else:
        currentP = ezca['SUS-' + SUS + '_M1_OPTICALIGN_P_OFFSET']
        currentY = ezca['SUS-' + SUS + '_M1_OPTICALIGN_Y_OFFSET']
    
    # calculate the distance from the good value
    distance = sqrt( (currentP - pit_good ) **2 + (currentY - yaw_good)**2)

    # allow a 3 urad deviation just in case one wants to try a slight different setting
    # changed to 0.01
    return distance < 10

#################################################
# centering function using GigE camera
#################################################
def GigE_centering(actuation_name, sensor_name, target_value, rate_value): 
    # move actuation point
    ezca[actuation_name] += rate_value * (target_value - ezca[sensor_name])
    # return the distance to the target
    return ezca[sensor_name] - target_value

# check if BS is loud in angle
def is_bs_loud():
    global BS_OPLEV_PIT_HIGH
    global BS_OPLEV_YAW_HIGH
    
    # monitor the standard deviation of BS angular motion
    PITdev = cdsutils.avg(0.2, 'SUS-BS_M3_OPLEV_PIT_OUT', stddev=True)[1]
    YAWdev = cdsutils.avg(0.2, 'SUS-BS_M3_OPLEV_YAW_OUT', stddev=True)[1]

    return PITdev > BS_OPLEV_PIT_HIGH or YAWdev > BS_OPLEV_YAW_HIGH

#################################################
# bs oplev checker
#################################################
class bs_checker(GuardStateDecorator):
    def pre_exec(self):
        if is_bs_loud():
            notify('BS is too loud in angle !')
            ezca['LSC-MICH_GAIN'] = 0
            return 'BS_TOO_LOUD'

##################################################
# TMSX(Y) alignment using ITMX(Y) Camera
##################################################
# user should pre-define the following variables when making an instance
# yPosTarget
# xPosTarget
# arm
# xCal
# yCal
# mu
# GoodError
def generate_TMSXY():
    class ALIGN_TMSXY(GuardState):
        request = False

        def main(self):

            self.tmsTrampPit = 'SUS-TMS%s_M1_OPTICALIGN_P_TRAMP' %(self.arm)
            self.tmsTrampYaw = 'SUS-TMS%s_M1_OPTICALIGN_Y_TRAMP' %(self.arm)
            self.tmsBiasPit = 'SUS-TMS%s_M1_OPTICALIGN_P_OFFSET' %(self.arm)
            self.tmsBiasYaw = 'SUS-TMS%s_M1_OPTICALIGN_Y_OFFSET' %(self.arm)
            self.itmCameraX = 'CAM-ITM%s_X'%(self.arm)
            self.itmCameraY = 'CAM-ITM%s_Y'%(self.arm)

            # Set camera exposure to 30000
            ezca['CAM-ITM%s_EXP'%(self.arm)] = 30000

            # Set up ramping time to 1 sec before updating the offsets
            ezca[self.tmsTrampPit] = 1
            ezca[self.tmsTrampYaw] = 1

            # Initialize position error, pix, timer
            self.error = 10
            self.timer['TMSmove'] = 0
            self.NoTRIALs = 0

        def run(self):
            if self.timer['TMSmove']:  # Allow time for TMS to move ( 1sec default)
                # move the spot on ITM 
                self.errorx = GigE_centering(self.tmsBiasYaw,       # actuation point
                                             self.itmCameraX,       # error signal
                                             self.xPosTarget,       # target value
                                             (self.mu)/(self.xCal)  # calibration and adoption rate
                                              )
                self.errory = GigE_centering(self.tmsBiasPit,       # actuation point
                                             self.itmCameraY,       # error signal
                                             self.yPosTarget,       # target value
                                             (self.mu)/(self.yCal)  # calibration and adoption rate
                                              )

                # Compute distance to the target
                self.error = sqrt( pow(self.errorx, 2) + pow(self.errory, 2))

                # Allow time for TMSX to move
                self.timer['TMSmove'] = 1
                log('######### Current distance to the target is %.5f pixels' %
                    (self.error))
                self.NoTRIALs += 1

            # exit this state if error is smaller than 1 pixel
            if self.error <= self.GoodError:
                return True
            # exit when the pixel does not converge
            if self.NoTRIALs > 200:
                notify('ERROR: the number of iterations exceeded 200.')
                return 'FAULT'

    return ALIGN_TMSXY

#################################################################
# pinting ITM to Baffle PD
#################################################################
# do not forget to set self.arm, self.PDnum and self.SUSmove when making an instance
def gen_POINT_ITM_TO_BPD():
    class POINT_ITM_TO_BPD(GuardState):
        request = False

        def main(self):
            # set channel names. Note that self.arm must be speficied when instanciated
            self.itm = 'ITM%s'%(self.arm)
            self.itmM0 = 'SUS-ITM%s_M0_'%(self.arm)
            self.itmBiasPit = 'SUS-ITM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.itmBiasYaw = 'SUS-ITM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.itmTestBiasPit = 'SUS-ITM%s_M0_TEST_P_OFFSET'%(self.arm)
            self.itmTestBiasYaw = 'SUS-ITM%s_M0_TEST_Y_OFFSET'%(self.arm)
            self.BPD1power = 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum)
            
            log('########  setting up %s'%(self.sus))
            # Set offset ramp times
            ezca[self.itmM0 + 'TEST_P_TRAMP'] = 5
            ezca[self.itmM0 + 'TEST_Y_TRAMP'] = 5
            ezca[self.itmM0 + 'OPTICALIGN_P_TRAMP'] = 1
            ezca[self.itmM0 + 'OPTICALIGN_Y_TRAMP'] = 1

            # baffle PD
            log('######## Pointing %s to BPD%d'%(self.sus, self.PDnum))
            ezca[self.itmTestBiasPit] = self.itmOffsetPitch
            ezca[self.itmTestBiasYaw] = self.itmOffsetYaw
            ezca.switch(self.itmM0 + 'TEST_P', 'OFFSET', 'OUTPUT', 'ON')
            ezca.switch(self.itmM0 + 'TEST_Y', 'OFFSET', 'OUTPUT', 'ON')

            # waite for SUS to move. SUSmove must be specified when instanciated
            self.timer['SUSmove'] = self.SUSmove

        def run (self):
            if self.timer['SUSmove']:
                return True

    return POINT_ITM_TO_BPD

#################################################################
# finding max power in a baffle pd
#################################################################
def generate_FIND_MAX_BPDPOWER():
    class FIND_MAX_BPDPOWER(GuardState):
        request = False

        def main(self):
            # define some names which are used frequently
            self.sus = 'ITM%s'%(self.arm)
            self.susBiasPit = 'SUS-ITM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ITM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.BPDchannel = 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum)
            
            log('######## Finding max point on BPD ... ')
            self.powerBPD = sweepQuad(self.sus, self.BPDchannel, 3)
            log('########  PD power is at %f mW'%(self.powerBPD))
    
            # Read current TMSX offsets
            if self.arm == 'X':
                global ITMX_BPD1_P
                global ITMX_BPD1_Y
                ITMX_BPD1_P = ezca[self.susBiasPit]
                ITMX_BPD1_Y = ezca[self.susBiasYaw]
            elif self.arm == 'Y':
                global ITMY_BPD1_P
                global ITMY_BPD1_Y
                ITMY_BPD1_P = ezca[self.susBiasPit]
                ITMY_BPD1_Y = ezca[self.susBiasYaw]
                log('### BPD1 maximized when ITMY pit is at %f'%(ITMY_BPD1_P))
                log('### BPD1 maximized when ITMY yaw is at %f'%(ITMY_BPD1_Y))

            if self.powerBPD < self.GoodPower:
                notify('BAFFLEPD%d is too low! Align %s manually'%(self.PDnum, self.sus))
                return 'MANUAL_ITM%s'%(self.arm)
            return True
    return FIND_MAX_BPDPOWER
                
#################################################################
# check the power on a second baffle PD
#################################################################
def generate_CHECK_SECOND_BPD():
    class CHECK_SECOND_BPD(GuardState):
        request = False

        def main(self):
            # define some names which are used frequently
            #[FIXME] can we avoid repeating calling these ?
            self.sus = 'ITM%s'%(self.arm)
            self.susM0 = 'SUS-ITM%s_M0_'%(self.arm)
            self.susBiasPit = 'SUS-ITM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ITM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.BPDchannel = 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum)
            
            log('######## Align %s on '%(self.sus))

            # Read current TMS offsets
            self.BPD2Pitch = ezca[self.susBiasPit]
            self.BPD2Yaw = ezca[self.susBiasYaw]

            if self.arm == 'X':
                global ITMX_BPD1_P
                global ITMX_BPD1_Y
                self.BPD1Pitch = ITMX_BPD1_P
                self.BPD1Yaw = ITMX_BPD1_Y
            elif self.arm == 'Y':
                global ITMY_BPD1_P
                global ITMY_BPD1_Y
                self.BPD1Pitch = ITMY_BPD1_P
                self.BPD1Yaw = ITMY_BPD1_Y
                log('BPD1_P = %f'%(ITMY_BPD1_P))
                log('BPD1_P = %f'%(ITMY_BPD1_Y))
         
            # check the BPD power
            self.BPDpower = cdsutils.avg(1, 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum))
            log('BPD power is at %f'%(self.BPDpower))

            # move ITM to a good position
            ezca[self.susBiasPit] = self.BPD1Pitch
            ezca[self.susBiasYaw] = self.BPD1Yaw

            # check the power on the PD 
            if self.BPDpower > self.GoodPower: 
                # Centering on the optic
                log('BPD power is good at %f'%(self.BPDpower))
            else:
                notify('PD power is too low !')
                log('PD power is loo low. It is now %f'%(self.BPDpower))

            # Turn off test offsets
            ezca.switch(self.susM0 + 'TEST_P', 'OFFSET', 'OFF')
            ezca.switch(self.susM0 + 'TEST_Y', 'OFFSET', 'OFF')
            return True
    return CHECK_SECOND_BPD


#################################################################
# Manual Alignment of ITMX
#################################################################
def generate_MANUAL_ITM():
    class MANUAL_ITM_GEN(GuardState):
        request = False

        def run(self):
            self.BPDpower = cdsutils.avg(1, 'AOS-ETM%s_BAFFLEPD_%d_POWER'%(self.arm, self.PDnum))
            if self.BPDpower >= self.GoodPower:
                return 'POINT_ITM%s_TO_BPD1'%(self.arm)

    return MANUAL_ITM_GEN

#################################################################
#  a function to generate ALIGNED states
#################################################################
def generate_ALIGNED():
    class ALIGNED_GEN(GuardState):
        def main(self):
            log('saving %s'%self.SUS)

            if self.SUS == 'PR2IM4':
                subprocess.call([align_save, 'PR2', 'IM4'])
            else:
                subprocess.call([align_save, self.SUS])
            return True
    return ALIGNED_GEN

#################################################################
# ETM alignment using ITMX Camera without green locked
#################################################################
def generate_ALIGNING_ETM():
    class ALIGNING_ETM(GuardState):
        request = False

        def main(self):
            # define names
            self.sus = 'ETM%s'%(self.arm)
            self.susM0 = 'SUS-ETM%s_M0_'%(self.arm)
            self.susBiasPit = 'SUS-ETM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ETM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.itmCameraX ='CAM-ITM%s_X'%(self.arm)
            self.itmCameraY ='CAM-ITM%s_Y'%(self.arm)

            # Set camera exposure to 30000
            ezca['CAM-ITMX_EXP'] = 30000

            # Set up ramping time to 1 sec before updating the offsets
            ezca[self.susM0 + 'OPTICALIGN_Y_TRAMP'] = 1
            ezca[self.susM0 + 'OPTICALIGN_P_TRAMP'] = 1

            log('######## Turn off greed PDH servo')
            ezca['ALS-%s_REFL_SERVO_IN1EN'%(self.arm)] = 0

            # Initialize position error, pix, # of trials, coarseness
            self.timer['ETMmove'] = 0
            self.NoTRIALs = 0
            self.coarseness = 'COARSE'

        def run(self):
            if self.timer['ETMmove']:
                # Allow time for ETMX to move
                self.timer['ETMmove'] = 1

                # Update YAW
                self.errorx = GigE_centering(self.susBiasYaw,       # actuation point
                                             self.itmCameraX,       # error signal
                                             self.itmGreenX,       # target value
                                             (self.mu)/(self.xCal)  # calibration and adoption rate
                                              )
                # Update PIT
                self.errory = GigE_centering(self.susBiasPit,       # actuation point
                                             self.itmCameraY,       # error signal
                                             self.itmGreenY,       # target value
                                             (self.mu)/(self.yCal)  # calibration and adoption rate
                                              )

                # Compute distance to the target
                self.error = sqrt(pow(self.errorx, 2) + pow(self.errory, 2))
                log('Current distance to the target is %.5f pixels' % (self.error))
                self.NoTRIALs += 1

                # switch the coarseness if error is smaller than GoodError
            if self.error <= self.GoodError:
                return True
 
            if self.NoTRIALs > 200:
                notify('Number of trials exceeded 200 times.')
                return 'FAULT'
    return ALIGNING_ETM

#################################################################
# ETM alignemnt with green locked
#################################################################
def generate_ALIGNING_ETM_GREEN_LOCKED():
    class ALIGNING_ETM_GREEN_LOCKED(GuardState):
        request = False
        def main(self):
            # define names
            self.sus = 'ETM%s'%(self.arm)
            self.susM0 = 'SUS-ETM%s_M0_'%(self.arm)
            self.susBiasPit = 'SUS-ETM%s_M0_OPTICALIGN_P_OFFSET'%(self.arm)
            self.susBiasYaw = 'SUS-ETM%s_M0_OPTICALIGN_Y_OFFSET'%(self.arm)
            self.itmCameraX ='CAM-ITM%s_X'%(self.arm)
            self.itmCameraY ='CAM-ITM%s_Y'%(self.arm)

            log('######## Turn on greed PDH servo')
            ezca['ALS-%s_REFL_SERVO_IN1EN'%(self.arm)] = 1

            # Reduce adaptation rate
            self.timer['ETMmove'] = 5
            self.NoTRIALs = 0

        def run(self):
            if self.timer['ETMmove']:

                # Update YAW
                self.errorx = GigE_centering(self.susBiasYaw,       # actuation point
                                             self.itmCameraX,       # error signal
                                             self.itmGreenX,       # target value
                                             (self.mu)/(self.xCal)  # calibration and adoption rate
                                              )
                # Update PIT
                self.errory = GigE_centering(self.susBiasPit,       # actuation point
                                             self.itmCameraY,       # error signal
                                             self.itmGreenY,       # target value
                                             (self.mu)/(self.yCal)  # calibration and adoption rate
                                              )

                # Compute distance to the target
                self.error = sqrt(pow(self.errorx, 2) + pow(self.errory, 2))
                log('Current distance to the target is %.5f pixels' % (self.error))

                # Allow time for ETMX to move
                self.timer['ETMmove'] = 1
                self.NoTRIALs += 1

            # finsh the alignment when error is less than GoodError
            if self.error <= self.GoodError:
                return True

            # go to FAULT
            if self.NoTRIALs > 200:
                notify('Number of trials exceeded 200 times.')
                return 'FAULT'
    return ALIGNING_ETM_GREEN_LOCKED


#################################################################
# maximize the green TR
#################################################################
def generate_MAXIMIZING_TR():
    class MAXIMIZING_TR(GuardState):
            request = False
            def main(self):
                self.sus = 'ETM%s'%(self.arm)
                log('maximizing power . . . ')
                # power optimization
                sweepQuad(self.sus, 'ALS-C_TR%s_A_LF_OUTPUT'%(self.arm), 0.5)
                return True
    return MAXIMIZING_TR

#################################################################
# LOCK an arm
#################################################################
def generate_LOCKING_ARM():
    class LOCKING_ARM_GEN(GuardState):
        request = False

        def main(self):
            # AS45I -> DARM_IN
            ezca['LSC-PD_DOF_MTRX_7_18'] = 1
            ezca['LSC-PD_DOF_MTRX_7_19'] = 0
            ezca['LSC-POW_NORM_MTRX_7_18'] = 0
            ezca['LSC-YARM_GAIN'] = 5
            ezca['LSC-DARM_GAIN'] = 0
            ezca.switch('LSC-YARM', 'FM2', 'FM4', 'FM5', 'FM6', 'INPUT', 'OUTPUT', 'ON')

            self.timer['LOCKwait'] = 60
            log('locking %s arm . . . '%(self.arm))

        def run(self):
            if arm_locked(which='X', trig = self.lockthresh):
                return True
            # [FIXME]: maybe we need a dedicated fault state for arm locking
            if self.timer['LOCKwait']:
                notify('%s arm did not lock. Manually lock it'%(self.arm))
                return 'MANUAL_XARM'
    return LOCKING_ARM_GEN

#################################################################
# Manual lock X arm
#################################################################
class MANUAL_XARM(GuardState):
    request = False
    def run(self):
    # [FIXME] is this state useful ?
        if arm_locked(which = 'X', trig = self.lockthresh): 
            return True

#################################################################
# Manual lock X arm
#################################################################
class XARM_LOCKED(GuardState):
    request = True
    def main(self):
           return


#################################################################
# PR2, IM4 alignment
# Since this is special, this is not wrapped by a define 
#################################################################
class ALIGNING_PR2_IM4(GuardState):
    request = False

    def main(self):
        log("Running REFL WFS for IM4 and PR2...")
        subprocess.call(pr2im4_alignment, shell=True)
        ezca.switch('LSC-YARM', 'OUTPUT', 'OFF')
        return True



#################################################################
# LOCK Michelson using AS45Q
#################################################################
class LOCKING_MICH(GuardState):
    request = False

    def main(self):
        # misalign PRM
        ezca['SUS-PRM_M1_OPTICALIGN_P_TRAMP'] = 5
        ezca['SUS-PRM_M1_OPTICALIGN_Y_TRAMP'] = 5
        ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = 0
        ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = 0

        # AS45Q -> MICH
        for jj in range(1,19):
            ezca['LSC-PD_DOF_MTRX_2_%d'%jj] = 0
        ezca['LSC-PD_DOF_MTRX_2_19'] = 1

        # Remove triggering
        ezca['LSC-MICH_TRIG_THRESH_ON'] = -1
        ezca['LSC-TRIG_MTRX_3_2'] = 0

        # Remove filter triggering
        ezca['LSC-MICH_MASK_FM2'] = 0
        ezca['LSC-MICH_MASK_FM6'] = 0
        ezca['LSC-MICH_MASK_FM7'] = 0
        ezca['LSC-MICH_MASK_FM8'] = 0

        # disable boosts and notches
        ezca.switch('LSC-MICH', 'FMALL', 'OFF')
        ezca.switch('LSC-MICH', 'FM1', 'FM3', 'FM6', 'FM7', 'ON')

        # Ramp MICH gain
        ezca.switch('LSC-MICH', 'OUTPUT', 'ON')
        ezca['LSC-MICH_TRAMP'] = 3
        ezca['LSC-MICH_GAIN'] = 3000

        # initialize ASDC value
        self.dark = 1000

    @bs_checker
    def run(self):
        self.dark = cdsutils.avg(1, 'LSC-ASAIR_A_LF_OUTPUT')

        # if an averaged AS_LF is lower than 10, move on to ALIGN_BS
        if self.dark < self.ASDCgood:
            # Turn integrators and notches on
            ezca.switch('LSC-MICH', 'FM2','ON')
            ezca['CAM-AS_EXP'] = 10000
            return True

#################################################################
# BS is too loud
#################################################################
class BS_TOO_LOUD (GuardState):
    request = False
    def main(self):
        ezca['CAM-AS_EXP'] = 1000
        log('wait for BS to settle down ...')
    def run(self):
        if not is_bs_loud():
            return True

#################################################################
# MICH locked
#################################################################
class MICH_LOCKED(GuardState):
    request = True
    @bs_checker
    def main(self):
        log('MICH is locked') 
    @bs_checker
    def run(self):
        pass
        return 

#################################################################
# BS alignment using AS WFS
#################################################################
class WFSING_BS(GuardState):
    request = False
    @bs_checker
    def main(self):
        log('######## BS Alignment with AS WFS')
        # Set camera exposure to 100000
        ezca['CAM-AS_EXP'] = 100000

        # use existing script
        subprocess.call(bs_alignment, shell=True)

        # Unlock MICH
        # Turn integrators and notches off
        ezca.switch('LSC-MICH', 'FM2', 'FM6', 'FM7', 'FM8', 'OFF')

        # Ramp MICH gain to 0
        ezca['LSC-MICH_GAIN'] = 0
        ezca['LSC-MICH_TRAMP'] = 1

        # Set camera exposure back to 1000
        ezca['CAM-AS_EXP'] = 1000

        # Add filter triggering
        ezca['LSC-MICH_MASK_FM2'] = 1
        ezca['LSC-MICH_MASK_FM6'] = 1
        ezca['LSC-MICH_MASK_FM7'] = 1
        ezca['LSC-MICH_MASK_FM8'] = 1

        # Return triggering
        ezca['LSC-MICH_TRIG_THRESH_ON'] = 800
        ezca['LSC-TRIG_MTRX_3_2'] = 1

        # REFL45Q -> MICH
        ezca['LSC-PD_DOF_MTRX_2_9'] = 1
        ezca['LSC-PD_DOF_MTRX_2_19'] = 0
        return True

#################################################################
# lock SRY
#################################################################
class LOCK_SRY(GuardState):
    request = False

    def main(self):
        ezca['SUS-SRM_M1_OPTICALIGN_P_TRAMP'] = 1
        ezca['SUS-SRM_M1_OPTICALIGN_Y_TRAMP'] = 1

        # Lock SRY
        # REFL9 -> SRCL
        ezca['LSC-PD_DOF_MTRX_4_6'] = 1
        ezca['LSC-PD_DOF_MTRX_4_8'] = 0

        # SRCL servo
        ezca.switch('LSC-SRCL', 'FM3', 'FM7', 'FM10', 'OUTPUT', 'INPUT', 'ON')
        ezca['LSC-SRCL_RSET'] = 2

        # Remove triggeing
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = -1
        ezca['LSC-TRIG_MTRX_4_2'] = 0

        # Remove filter triggering
        ezca['LSC-SRCL_MASK_FM2'] = 0

        # Ramp up SRCL gain
        log('######## locking SRY ....')
        ezca['LSC-SRCL_TRAMP'] = 5
        ezca['LSC-SRCL_GAIN'] = -4000
        mysleep(5)
        ezca['LSC-SRCL_GAIN'] = -40000
        mysleep(7)

    def run(self):
        # Turn on integrator
        # [FIXME] we need a lock checker
        ezca.switch('LSC-SRCL', 'FM2', 'ON')
        return True

#################################################################
# SRY locked
#################################################################
class SRY_LOCKED(GuardState):
    request = True
    def main(self):
        return

#################################################################
# SRM alignment using AS Camera
#################################################################
class ALIGN_SRM(GuardState):
    request = False

    def main(self):
        log('######## SRM alignment using REFL WFS')
        subprocess.call(sry_alignment, shell=True)

        # Unlock SRY
        # Turn the integrator off
        ezca.switch('LSC-SRCL', 'FM2', 'OFF')

        # Ramp the gain down
        ezca['LSC-SRCL_GAIN'] = 0
        ezca['LSC-SRCL_TRAMP'] = 1

        # Set up triggering
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = 500
        ezca['LSC-TRIG_MTRX_4_2'] = 1

        # Set up filter triggering
        ezca['LSC-SRCL_MASK_FM2'] = 1

        # REFL45I -> SRCL
        ezca['LSC-PD_DOF_MTRX_4_6'] = 0.42
        ezca['LSC-PD_DOF_MTRX_4_8'] = 1
        return True
