# ias_quad_sus.py
#
# $Id: ias_quad_sus.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/ias_quad_sus.py $


from numpy import argmax
from time import sleep

"""
example usage 1:
xarm = QUADsuspension('ITMX', 'ETMX')	
for the purpose of diff/common angle scans

exmaple usage 2:
xarm = QUADsuspension('ITMX')
"""
class QUADsuspension():
	def __init__(self, A_name, B_name=None):
		# suspension names
		self.A_name = A_name 
		self.B_name = B_name

		# prepare empty lists
		self.calP = []
		self.calY = []
		self.initAngleP = []
		self.initAngleY = []

		# initialize some relavent values
		self.set_ramp_times(A_name, 15.0)
		self.save_init_angles(A_name)
		self.get_calibration(A_name)

		# repeat the same if B_name is called
		if not B_name==None:
			self.save_init_angles(B_name)
			self.set_ramp_times(B_name, 15.0)
			self.get_calibration(B_name)

	# save the initiali alignment
	def save_init_angles(self, name):
		self.initAngleP.append( ezca['SUS-%s_M0_OPTICALIGN_P_OFFSET'%name] )
		self.initAngleY.append( ezca['SUS-%s_M0_OPTICALIGN_Y_OFFSET'%name] )

	# Set ramp times
	def set_ramp_times(self, name, tramp=15.0):
		for DOF in ('P', 'Y'):
			ezca['SUS-%s_M0_OPTICALIGN_%s_TRAMP'%(name, DOF)] = tramp
	
	# get calibration
	def get_calibration(self, name):
		self.calP.append( ezca['SUS-%s_M0_OPTICALIGN_P_GAIN'%name] )
		self.calY.append( ezca['SUS-%s_M0_OPTICALIGN_Y_GAIN'%name] )

	# sweep the suspension to find a max in PDchan, then returns the best angle
	def sweep(self, name, sweep_range, PDchan, PorY):
		chan_name = 'SUS-%s_M0_OPTICALIGN_%s'%(name, PorY)

		# check the initial values
		if name == self.A_name and PorY == 'Y':
			initAngle = self.initAngleY[0]
			cal = self.calY[0]
		elif name == self.B_name and PorY == 'Y':
			initAngle = self.initAngleY[1]
			cal = self.calY[1]
		elif name == self.A_name and PorY == 'P':
			initAngle = self.initAngleP[0]
			cal = self.calP[0]
		elif name == self.B_name and PorY == 'P':
			initAngle = self.initAngleP[0]
			cal = self.calP[1]

		# go negative
		ezca[chan_name + '_OFFSET'] = initAngle - sweep_range

		# Get data
		dataAngle, dataPD = [], []
		while ezca[chan_name + '_OUTPUT'] > (initAngle - sweep_range) * cal + 1e-2:
			current_angle = ezca[chan_name + '_OUTPUT'] / cal

			# store the value
			dataAngle.append(current_angle)
			dataPD.append( ezca[PDchan] )

		# Go positive
		ezca[chan_name + '_OFFSET'] = initAngle + sweep_range

		# Get data
		while ezca[chan_name + '_OUTPUT'] < (initAngle + sweep_range) * cal - 1e-2:
			current_angle = ezca[chan_name + '_OUTPUT'] / cal
	
			# store the value
			dataAngle.append(current_angle)
			dataPD.append(ezca[PDchan])

		# Find max power
		peak = argmax(dataPD)
		return dataAngle[peak]

	# a similar sweep function, but capable of doing differential and common
	# between A_name and B_name suspensions.
	def dual_sweep(self, sweep_range, PDchan, PorY, B_gain):
		A_chan = 'SUS-%s_M0_OPTICALIGN_%s'%(self.A_name, PorY)
		B_chan = 'SUS-%s_M0_OPTICALIGN_%s'%(self.B_name, PorY)

		# check the initial values
		if PorY == 'Y':
			initAngle = self.initAngleY
			cal = self.calY
		elif PorY == 'P':
			initAngle = self.initAngleP
			cal = self.calP

		# go negative in A_chan
		ezca[A_chan + '_OFFSET'] = initAngle[0] - sweep_range
		ezca[B_chan + '_OFFSET'] = initAngle[1] - B_gain * sweep_range

		# Get data
		dataAngle, dataPD = [], []
		while ezca[A_chan + '_OUTPUT'] > (initAngle[0] - sweep_range) * cal[0] + 1e-2:
			current_angle = ezca[A_chan + '_OUTPUT'] / cal[0]

			# store the value
			dataAngle.append(current_angle)
			dataPD.append( ezca[PDchan] )

		# Go positive
		ezca[A_chan + '_OFFSET'] = initAngle[0] + sweep_range

		# Get data
		while ezca[A_chan + '_OUTPUT'] < (initAngle[0] + sweep_range) * cal[0] - 1e-2:
			current_angle = ezca[A_chan + '_OUTPUT'] / cal[0]
	
			# store the value
			dataAngle.append( current_angle )
			dataPD.append( ezca[PDchan] )

		# Find max power
		peak = argmax(dataPD)

		# caluclte the best angles
		dt = dataAngle[peak] - initAngle[0]
		best_angle_A_chan = dataAngle[peak]
		best_angle_B_chan = initAngle[1] + dt * B_gain

		# return good value for both angles
		return (best_angle_A_chan, best_angle_B_chan)
	
	# set the angle	
	def set_angle(self, name, PorY, value):
		ezca['SUS-%s_M0_OPTICALIGN_%s_TRAMP'%(name, PorY)] = 8
		ezca['SUS-%s_M0_OPTICALIGN_%s_OFFSET'%(name, PorY)] = value

	#set some setting values back to nominal
	def clean_up(self):
		# set the values back to 5 sec
		self.set_ramp_times(self.A_name, 5.0)
		if not self.B_name==None:
			self.set_ramp_times(self.B_name, 5.0)

